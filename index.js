// console.log("Robert California");

/*
	ES6 Updates

	ES6 is one of the latest versions of writing JavaScript and in face is one of the latest major update to JS.

	let, const - are ES6 updates to update the standard of creating variables.
	var - was the keyword used previously before ES6.

*/

// console.log(samplelet);
// let samplelet = "Sample";

// console.log(varSample);
// var varSample = "Hoist Me Up!";

// Exponent operator
let fivePowerOf3 = Math.pow(5,3);
console.log(fivePowerOf3);
//Math.pow() allows us tp get the result of a nubmer raised to the given exponent
//Math.pow(base,exponent);

// Exponent operators - ** - allows us to get the result of a number raised to a given exponent. it is used as an alternative to Math.pow()
let fivePowerOf2 = 5**2;
console.log(fivePowerOf2);

// Getting square root of a number using exponent operatror.
let squareRootOf4 = 4**.5;
console.log(squareRootOf4);

// Template Literals 
// "", '' - string literals
// how do we  combine strings?

let word1 = "JavaScript";
let word2 = "Java";
let word3 = "is";
let word4 = "not";

// let sentence1 = word1 + " " + word3 + " " + word4 + " " + word2 + ".";
// console.log(sentence1);

// `` - backticks - template literals - allows us to create string using `` and easily embed JS Expressions

//${} is used in template literals to embed JS expressions and variables.
//${} - placeholder
let sentence1 = `${word1} ${word3} ${word4} ${word2}`;
console.log(sentence1);

let sentence2 = `${word2} is an OOP Language.`
console.log(sentence2);

// Template literals can also be used to embed JS expressions
let sentence3 = `The sum of 15 and 25 is ${15+25}`;
console.log(sentence3);

let user1 = {
	name: "Michael",
	position: "Manager",
	income: 90000,
	expenses: 50000
}
console.log(`${user1.name} is a ${user1.position}`)
console.log(`His income is ${user1.income} and expenses at ${user1.expenses}. his current blance is ${user1.income - user1.expenses}`);

// Destructuring Arrays and objects
// destructuring will allow us to save array elements or object properties into new variables without having to create/initialize with accessing items/properties one by one 

let array1 = ["Curry", "Lillard", "Paul", "Irving"];

// let player1 = array1[0];//curry
// let player2 = array1[1];//lillard
// let player3 = array1[2];//paul
// let player4 = array1[3];//irving

// console.log(player1, player2, player3, player4)

// Array destructuring is when we save array items into variables.
// in arrays, order matters and that goes the same for destructuring:
let [player1, player2, player3, player4] = array1;

console.log(player1);
console.log(player4);

let array2 = ["Jokic", "Embiid", "Anthony-Towns", "Davis"]

let [center1,,,center2] = array2;
console.log(center1);
console.log(center2);

// Object Destructuring
// Object Destructuing allows us to get the value of property and save in a variable of the same name.

let pokemon1 = {
	name: "Bulbasaur",
	type: "Grass",
	level: 10,
	moves: ["Razor Leaf", "Tackle", "Leech See"]
}

//Order does not matter in destructuring object.
//what matters are the keys/properties name.
//the variable name should exactly math the property name.
let {type,level,name,moves,personality} = pokemon1;

console.log(type);
console.log(level);
console.log(moves);
console.log(name);
console.log(personality);//undefined

function greet(object){

	/*
		When passed user1, object now contains the key-value pairs of user1 = {
			name: "Michael",
			position: "Manager",
			income: 90000,
			expenses: 50000
		}
	*/

	let {name} = object; // allowed us to get object.name and save it as name

	console.log(`Hello! ${name}`);
	console.log(`${name} is my friend!`);
	console.log(`Good Luck, ${name}!`);
	// console.log(`position is ${position}`)
}
greet(user1);

// Mini-Activity:
// Destructure the following object and log the values of the productName and price in the console:

/*let product1 = {
	productName: "Safeguard Handsoap",
	description: "Liquid Handsoap by Safeguard.",
	price: 25,
	isActive: true

}

// Display the name and price in the console by destructuring.
// Send a screenshot of your output in the hangouts.
let {productName,price} = product1;
console.log(`Name: ${productName}, Price: ${price}`);
//or
function displayNamePrice (product){

	let {productName,price} = product1;
	console.log(`Name: ${productName}, Price: ${price}`);
}
displayNamePrice(product1);*/


//Arrow functions
//Arrow functions are an alternative way of writing function in JS.
//However, there significant pros and cons between traditional and arrow function.

//traditional function
function displayMsg(){
	console.log(`Hello, World!`)
}

displayMsg();

// Arrow function
const hello = () =>{
	console.log(`Hello, Arrow!`);
}

hello();

//arrow functions with parameters:
//we don't usually use let keyword to assign our arrow function to avoid updating  the variable
const alertUser = (username) =>{
	console.log(`This is an alert for user ${username}`);
}

alertUser("James1991");

//Arrow and traditional functions are pretty much the same. They are functions. However, there are some key differences.

//Implicit Return - is the ability of an arrow function to the return value without the use of return keyword.

// traditional addNUm() function
function addNum(num1,num2){
	let result = num1 + num2;

 	return result;
}

let sum1 = addNum(5,10);
console.log(sum1);

//Arrow Function have implicit return. When an arrow function is written in one line, it can return value without return keyword.
//implicit returns will only work on arrow function written in 1 line and without {}
const addNumArrow = (num1,num2) => num1+num2;

let sum2 = addNumArrow(10,20);
console.log(sum2);

//if an arrow function is written in more than one line and with {} then we will need a return keyword:
const subNum = (num1,num2) => num1 - num2;


let difference = subNum(20,10);
console.log(difference);

// traditional functions vs arrow functions as object methods

let character1 = {
	name: "Cloud Strife",
	occupation: "SOLDIER",
	introduceName: function(){
		//in traditional function as a method
		//this refers to the object where the method is.
		console.log(`Hi! I'm ${this.name}`);
	},
	introduceJob:() =>{
		//in an arrow function as a method,
		//this actually refers to the global window object/the whole document.
		//console.log(`My job is ${this.occupation}`);
		//this is why it is not advisable to use arrow function as method.
		console.log(this)
	}
}

character1.introduceName();
character1.introduceJob();

/*

const sampleObj = {
	name: "Sample1",
	age: 25
}

sampleObj.name = "Smith";
console.log(sampleObj)

*/

//Class Based Object Blueprints
	//In JavaScript, Classes are templates of Objects
	//We can use classe to create objects following the structure of the class similar to a constructor function.

	//Constructor Function
	// function Pokemon(name,type,level){
	// 	this.name = name;
	// 	this.type = type;
	// 	this.level = level;
	// }
	// let pokemonInstance1 = new Pokemon("Pikachu", "Electric", 25);
	// console.log(pokemonInstance1);

	//With the advent of ES6, we are now introduced to a new way of creating objects with a blueprint with the use of Classes.

	class Car {
		constructor(brand,model,year){
			this.brand = brand;
			this.model = model;
			this.year = year;
		}
	}

	let car1 = new Car("Toyota", "Vios", "2002");
	let car2 = new Car("Cooper","Mini","1967"); 
	let car3 = new Car("Porsche","911","1968"); 

	console.log(car1);
	console.log(car2);
	console.log(car3);

	//mini-activity
	//translate the pokemon constructor function into a class constructor
	//create 2 new pokemons out of class constructor and save it in their variables
	//log the values in the console
	//share your output in the hangouts.


	class Pokemon{
		constructor(name,type,level){
			this.name = name;
			this.type = type;
			this.level = level;
		}
	}

	let pokemonClass1 = new Pokemon("Articuno", "Ice", 99)
	let pokemonClass2 = new Pokemon("Zapdos", "Electric", 99)

	console.log(pokemonClass1);
	console.log(pokemonClass2);



// Arrow Functions in Array Methods
let numArr = [2,10,3,10,5];

//Array Method with Traditional Function
// let reduceNumber = numArr.reduce(function(x,y){
// 	//Get the sum of all number in the array
// 	return x+y;
// })

//arrow function with return
// let reduceNumber = numArr.reduce((x,y) => {
// 	return x+y;
// })

//Reduce with implicit return
let reduceNumber = numArr.reduce((x,y) => x +y);

console.log(reduceNumber);

//Tip: if you are still getting confused on making arrow functions with array methods, first create the method with a traditional function, then translate that into arrow function:
//let mappedNum = numArr.map(function(num){
// 	return num*2;
// })

let mappedNum = numArr.map((num) =>{
	return num*2;
})

console.log(mappedNum);